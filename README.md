# myPortfolio

This is my personal portfolio website

## Development

### Develop with hot reloads (webpack)

```
cd web
npm run serve
```

### Run web site

Build website and start server

```
npm run start
```

## Testing

```
npm run test
```
