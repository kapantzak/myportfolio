const express = require("express");
const path = require("path");
const compression = require("compression");
const helmet = require("helmet");
const bodyParser = require("body-parser");
const app = express();
const port = process.env.PORT || "8080";

app.use(helmet());
app.use(compression());
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true
  })
);
app.use(express.static(path.join(__dirname, "web/dist")));

app.use((req, res) => {
  const routes = ["/", "/home", "/about", "/projects", "/posts", "/contact"];
  if (routes.indexOf(req.url) === -1) {
    res.status(404).sendFile(path.resolve("web/dist/404.html"));
  } else {
    res.status(200).sendFile(path.resolve("web/dist/index.html"));
  }
});

app.listen(port, () => {
  console.log(`Listening on port ${port}`);
});
