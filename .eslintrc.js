module.exports = {
    parser: "babel-eslint",
    plugins: ["prettier"],
    rules: {
      "prettier/prettier": "error"
    },
    parserOptions: {
      ecmaFeatures: {        
        modules: true
      }
    },
    extends: [
      "eslint:recommended",      
      "airbnb-base",
      "plugin:prettier/recommended"
    ]
};