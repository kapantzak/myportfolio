import { describe, it } from "mocha";
import { assert } from "chai";
import { imgPath, projects, getProjects } from "../src/dataStore/projects";

describe("projects array", () => {
  describe("Check titles", () => {
    projects.forEach((x, index) => {
      it(`Project in position ${index} should have a title of type string`, () => {
        assert.exists(x.title);
        assert.typeOf(x.title, "string");
      });
    });
  });

  describe("Check descriptions", () => {
    projects.forEach((x, index) => {
      it(`Project in position ${index} should have a description of type string`, () => {
        assert.exists(x.descr);
        assert.typeOf(x.descr, "string");
      });
    });
  });

  describe("Check image", () => {
    projects.forEach((x, index) => {
      it(`Project in position ${index} should have an image name of type string`, () => {
        assert.exists(x.img);
        assert.typeOf(x.img, "string");
      });
    });
  });
});

describe("getProjects()", () => {
  const projectsList = getProjects();
  it("Should return only active projects", () => {
    const inactive = projects.filter(x => x.active !== true);
    const expected = projects.length - inactive.length;
    const actual = projectsList.length;
    assert.equal(actual, expected);
  });

  describe("Image paths", () => {
    projectsList.forEach(x => {
      it("Should return full image source path", () => {
        const imageName = projects.find(p => p.title === x.title).img;
        const expected = x.projectFolder
          ? `${imgPath}${x.projectFolder}/${imageName}`
          : `${imgPath}${imageName}`;
        const actual = x.img;
        assert.equal(actual, expected);
      });
    });
  });
});
