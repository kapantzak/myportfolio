import { describe, it } from "mocha";
import { assert } from "chai";
import { skills, getSkills } from "../src/dataStore/skills";

describe("skills array", () => {
  describe("Check titles", () => {
    skills.forEach((x, index) => {
      it(`Skill in position ${index} should have a title of type string`, () => {
        assert.exists(x.title);
        assert.typeOf(x.title, "string");
      });
    });
  });
});

describe("getSkills()", () => {
  const skillsList = getSkills();
  it("Should return only active skills", () => {
    const inactive = skills.filter(x => x.active !== true);
    const expected = skills.length - inactive.length;
    const actual = skillsList.length;
    assert.equal(actual, expected);
  });
});
