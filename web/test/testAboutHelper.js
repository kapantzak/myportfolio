import { describe, it } from "mocha";
import { assert } from "chai";
import { getSkillBarWidth } from "../src/helpers/aboutSkillsHelper";

describe("getSkillBarWidth()", () => {
  [
    {
      expected: "0%",
      skill: {}
    },
    {
      expected: "0%",
      skill: {
        percentage: null
      }
    },
    {
      expected: "0%",
      skill: {
        percentage: 0
      }
    },
    {
      expected: "0%",
      skill: {
        percentage: -10
      }
    },
    {
      expected: "100%",
      skill: {
        percentage: 100
      }
    },
    {
      expected: "100%",
      skill: {
        percentage: 110
      }
    },
    {
      expected: "10%",
      skill: {
        percentage: 10
      }
    }
  ].forEach(x => {
    it(`Should return ${x.expected} when skill's percentage is ${x.skill.percentage}`, () => {
      const actual = getSkillBarWidth(x.skill);
      assert.equal(actual, x.expected);
    });
  });
});
