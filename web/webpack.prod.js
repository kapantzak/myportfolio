const path = require("path");
const merge = require("webpack-merge");
const common = require("./webpack.common");
const OfflinePlugin = require("offline-plugin");
const PUBLIC_PATH = "https://kapantzakis.gr/";

module.exports = merge(common, {
  mode: "production",
  output: {
    publicPath: PUBLIC_PATH
  },
  plugins: [
    new OfflinePlugin({
      externals: ["/"],
      ServiceWorker: {
        entry: path.resolve("service-worker-bootstrap.js"),
        prefetchRequest: { credentials: "same-origin" }
      }
    })
  ]
});
