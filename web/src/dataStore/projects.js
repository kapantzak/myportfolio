export const imgPath = "./img/projects/";

export const projects = [
  {
    title: "This website",
    descr:
      "My personal portfolio. A PWA written in vanilla Javascript and bundled with webpack",
    projectFolder: "myPortfolio",
    img: "portfolio.png",
    stack: ["Javascript", "HTML", "CSS", "Webpack", "Node.js"],
    images: [
      {
        src: "portfolio_2.png",
        text: "About",
        orientation: "horizontal"
      },
      {
        text: "HTML5 boilerplate template used for the HTML markup"
      },
      {
        src: "portfolio_7.png",
        text: "Skills",
        orientation: "horizontal"
      },
      {
        text: "Bold typography and vivid colors used"
      },
      {
        src: "portfolio_3.png",
        text: "Projects",
        orientation: "horizontal"
      },
      {
        src: "portfolio_6.png",
        text: "Project details",
        orientation: "horizontal"
      },
      {
        text: "All animations and transitions achieved with CSS3"
      },
      {
        src: "portfolio_4.png",
        text: "Posts",
        orientation: "horizontal"
      },
      {
        text: "Posts fetched from DEV Community API"
      },
      {
        src: "portfolio_5.png",
        text: "Contact",
        orientation: "horizontal"
      }
    ],
    links: [
      {
        text: "GitLab repo",
        type: "GitLab",
        url: "https://gitlab.com/kapantzak/myportfolio"
      }
    ],
    active: true
  },
  {
    title: "Attendance management system",
    descr:
      "A system that manages students attendance data, composed of a web client, a mobile app and an API.",
    projectFolder: "attendanceMonitoring",
    img: "attendanceMobile.png",
    stack: [
      "ASP.NET Core",
      "C#",
      "Typescript",
      "React",
      "Ionic",
      "Webpack",
      "MS SQL"
    ],
    images: [
      {
        src: "atms_0.png",
        text: "Intro screen",
        orientation: "vertical"
      },
      {
        text: "Mobile app is developed with Ionic framework"
      },
      {
        src: "atms_1.png",
        text: "Login screen",
        orientation: "vertical"
      },
      {
        src: "atms_2.png",
        text: "List of enrollments",
        orientation: "vertical"
      },
      {
        src: "atms_3.png",
        text: "App menu",
        orientation: "vertical"
      },
      {
        text: "Students can view their attendance logs for each enrolled course"
      },
      {
        src: "atms_4.png",
        text: "Details screen",
        orientation: "vertical"
      },
      {
        src: "atms_5.png",
        text: "Scan confirmation",
        orientation: "vertical"
      },
      {
        text:
          "Students can scan the QR code generated inside the class, in order to register their attendance"
      },
      {
        src: "atms_6.png",
        text: "Scanning QR code",
        orientation: "vertical"
      },
      {
        src: "web_professor_dashboard.jpg",
        text: "Professor's courses list",
        orientation: "horizontal"
      },
      {
        text:
          "Professors are able to have an overview of the students' attendance logs"
      },
      {
        src: "web_student_logs_overview.jpg",
        text: "Student's attendance logs",
        orientation: "horizontal"
      },
      {
        text: "Professors generate a unique QR code for each session"
      },
      {
        src: "web_professor_qrcode_display.jpg",
        text: "QR code generation",
        orientation: "horizontal"
      },
      {
        text:
          "Both of these systems (mobile app and web client) communicate with the same API (ASP.NET Core), which, in turn, executes the necessary database operations"
      }
    ],
    links: [
      {
        text: "Attendance Mobile App - Repo",
        type: "GitHub",
        url: "https://github.com/kapantzak/AttendanceMobileApp"
      },
      {
        text: "Attendance Web Client - Repo",
        type: "GitHub",
        url: "https://github.com/kapantzak/AttendanceWeb"
      },
      {
        text: "Attendance API - Repo",
        type: "GitHub",
        url: "https://github.com/kapantzak/AttendanceWebAPI"
      }
    ],
    active: true
  },
  {
    title: "text-typing",
    descr:
      "A javascript tool that applies the type writer effect to any DOM element, available as npm package",
    projectFolder: "textTyping",
    img: "text-typing.gif",
    stack: ["Javascript", "Rollup.js"],
    images: [
      {
        src: "text-typing.gif",
        text: "The type writer effect",
        orientation: "horizontal"
      },
      {
        text:
          "A javascript tool, with a simple promise-based API, that can be used to apply the type writer effect to any DOM element"
      },
      {
        code: `<p>(async () => {</p>
            <p class="sp-1">const el = document</p>
            <p class="sp-2">.getElementById("myHeading");</p>
            <p class="sp-1">const txt = textTyping(el);</p>            
            <p class="sp-1">await txt.typeText("Hello");</p>
          <p>})();</p>`
      },
      {
        text:
          "The modules are bundled with the use of Rollup.js bundler, a tool that produces smaller bundles than webpack"
      }
    ],
    links: [
      {
        text: "text-typing repo",
        type: "GitHub",
        url: "https://github.com/kapantzak/text-typing"
      },
      {
        text: "Import from npm",
        type: "npm",
        url: "https://www.npmjs.com/package/text-typing"
      }
    ],
    active: true
  },
  {
    title: "Search Area Control",
    descr:
      "A jQuery plugin that helps displaying, searching and selecting items in a tree structure",
    projectFolder: "searchAreaControl",
    img: "searchAreaControl.gif",
    stack: ["HTML", "CSS", "jQuery"],
    images: [
      {
        src: "sac_overview.png",
        text: "Search area",
        orientation: "vertical"
      },
      {
        text:
          "The plugin is initialized on specific DOM elements (usually on buttons or anchors). When user clicks the specified element, a search area is displayed in a popup"
      },
      {
        src: "sac_search_starts.png",
        text: "Search - Starts with",
        orientation: "horizontal"
      },
      {
        text:
          "User is able to select between three possible search types ('Starts with', 'Exists' and 'Regular expression'"
      },
      {
        src: "sac_search_exists.png",
        text: "Search - Exists",
        orientation: "vertical"
      },
      {
        text:
          "There are many available options to customize the behaviour of the plugin. There are also useful methods that can be called and various events that get dispatched in specific points of the plugin's lifecycle"
      }
    ],
    links: [
      {
        text: "GitHub repo",
        type: "GitHub",
        url: "https://github.com/kapantzak/SearchAreaControl"
      },
      {
        text: "Import from npm",
        type: "npm",
        url: "https://www.npmjs.com/package/searchareacontrol"
      },
      {
        text: "Demo page",
        type: "demo",
        url: "https://kapantzak.github.io/SearchAreaControl/"
      }
    ],
    active: true
  },
  {
    title: "Bar Indicator",
    descr:
      "A jQuery plugin that helps visualizing numeric data into bar charts",
    projectFolder: "barIndicator",
    img: "average.png",
    stack: ["HTML", "CSS", "jQuery"],
    images: [
      {
        src: "simple.png",
        text: "Default bar",
        orientation: "horizontal"
      },
      {
        code: `$("#elem").barIndicator();`
      },
      {
        text:
          "Visualize numeric data with beautiful HTML/CSS bars and customize the final result with a variety of options"
      },
      {
        src: "labelPosition.jpg",
        text: "Label positioning",
        orientation: "horizontal"
      },
      {
        text: "Provide custom colors for specific ranges"
      },
      {
        src: "colorRange.jpg",
        text: "Custom color ranges",
        orientation: "horizontal"
      },
      {
        text: "Set custom milestones to indicate special values"
      },
      {
        src: "milestones-compressor.gif",
        text: "Milestones",
        orientation: "horizontal"
      },
      {
        text:
          "Call various methods in order to animate the control, load new data or destroy the plugin"
      },
      {
        src: "methods-compressor.gif",
        text: "Methods",
        orientation: "horizontal"
      }
    ],
    links: [
      {
        text: "GitHub repo",
        type: "GitHub",
        url: "https://github.com/kapantzak/barIndicator"
      },
      {
        text: "Demo page",
        type: "demo",
        url: "http://kapantzak.github.io/barIndicator/"
      }
    ],
    active: true
  },
  {
    title: "jsTiles",
    descr:
      "A jQuery plugin that works alongside with Bootstrap and helps you create beautiful tiles",
    projectFolder: "jsTiles",
    img: "jsTiles.png",
    stack: ["HTML", "CSS", "Bootstrap", "jQuery"],
    images: [
      {
        src: "overview.gif",
        text: "Tiles",
        orientation: "horizontal"
      },
      {
        text:
          "Initialize the plugin and create beautiful tiles, providing various options to customize the layout and appearance of your app"
      },
      {
        src: "init_fade-compressor.gif",
        text: "Fade in effect",
        orientation: "horizontal"
      },
      {
        src: "init_scale-compressor.gif",
        text: "Scale effect",
        orientation: "horizontal"
      },
      {
        src: "init_slide-compressor.gif",
        text: "Slide effect",
        orientation: "horizontal"
      }
    ],
    links: [
      {
        text: "GitHub repo",
        type: "GitHub",
        url: "https://github.com/kapantzak/jstiles"
      },
      {
        text: "Demo page",
        type: "demo",
        url: "http://kapantzak.github.io/jstiles/"
      }
    ],
    active: true
  },
  {
    title: "Blog Tiles",
    descr: "A Bootstrap Template that uses tiles to display your blog posts",
    projectFolder: "blogTiles",
    img: "blogTiles.png",
    stack: ["HTML", "CSS", "Bootstrap", "jQuery"],
    images: [
      {
        src: "load-compressor.gif",
        text: "Loading posts",
        orientation: "vertical"
      },
      {
        text: "Apply specific css classes and create a beautiful blog"
      },
      {
        src: "scroll-compressor.gif",
        text: "Lazy loading",
        orientation: "vertical"
      },
      {
        text:
          "Information about the blog author can be viewd in the about section"
      },
      {
        src: "about-compressor.gif",
        text: "About section",
        orientation: "vertical"
      },
      {
        text: "Search archived posts, either by date or by tag name"
      },
      {
        src: "archive-compressor.gif",
        text: "Search older posts",
        orientation: "vertical"
      },
      {
        text: "Open the contact form from the top menu"
      },
      {
        src: "contact-compressor.gif",
        text: "Contact section",
        orientation: "vertical"
      }
    ],
    links: [
      {
        text: "GitHub repo",
        type: "GitHub",
        url: "https://github.com/kapantzak/BlogTiles"
      },
      {
        text: "Demo page",
        type: "demo",
        url: "http://kapantzak.github.io/BlogTiles/"
      }
    ],
    active: true
  }
];

export function getProjects() {
  return projects.reduce((acc, x) => {
    if (x.active) {
      const projectPath = x.projectFolder
        ? `${imgPath}${x.projectFolder}/`
        : imgPath;
      acc.push(
        Object.assign({}, x, {
          img: x.img ? `${projectPath}${x.img}` : null,
          images: x.images
            ? x.images.reduce((acc, i) => {
                const srcPath = i.src ? `${projectPath}${i.src}` : undefined;
                acc.push(
                  Object.assign({}, i, {
                    src: srcPath
                  })
                );
                return acc;
              }, [])
            : []
        })
      );
    }
    return acc;
  }, []);
}
