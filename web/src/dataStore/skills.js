export const skills = [
  {
    id: "3",
    title: "Javascript",
    percentage: 95,
    link: "https://developer.mozilla.org/el/docs/Web/JavaScript",
    order: 0,
    active: true
  },
  {
    id: "10",
    title: "React",
    percentage: 90,
    link: "https://reactjs.org/",
    order: 100,
    active: true
  },
  {
    id: "8",
    title: "Next.js",
    percentage: 80,
    link: "https://azure.microsoft.com/en-us/services/devops/",
    order: 120,
    active: true
  },
  {
    id: "12",
    title: "Typescript",
    percentage: 70,
    link: "https://www.typescriptlang.org/",
    order: 200,
    active: true
  },
  {
    id: "1",
    title: "HTML",
    percentage: 95,
    link: "https://www.w3.org/html/",
    order: 300,
    active: false
  },
  {
    id: "2",
    title: "CSS",
    percentage: 95,
    link: "https://www.w3.org/Style/CSS/",
    order: 400,
    active: true
  },
  {
    id: "4",
    title: "jQuery",
    percentage: 95,
    link: "https://jquery.com/",
    order: 500,
    active: true
  },
  {
    id: "11",
    title: "Ruby on Rails",
    percentage: 60,
    link: "https://rubyonrails.org/",
    order: 600,
    active: true
  },
  {
    id: "5",
    title: "C#",
    percentage: 65,
    link: "https://docs.microsoft.com/en-us/dotnet/csharp/index",
    order: 700,
    active: true
  },
  {
    id: "7",
    title: "Git",
    percentage: 85,
    link: "https://git-scm.com/",
    order: 800,
    active: true
  },
  {
    id: "6",
    title: "SQL",
    percentage: 40,
    link: "https://en.wikipedia.org/wiki/SQL",
    order: 900,
    active: true
  },
  {
    id: "9",
    title: "ASP.NET Core",
    percentage: 40,
    link: "https://dotnet.microsoft.com/apps/aspnet",
    order: 1100,
    active: false
  }
];

export function getSkills() {
  const arr = skills.reduce((acc, x) => {
    if (x.active) {
      acc.push(x);
    }
    return acc;
  }, []);
  // arr.sort((a, b) => a.order - b.order);
  return arr;
}
