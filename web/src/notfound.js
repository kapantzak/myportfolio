import { textTyping } from "text-typing";

async function init() {
  const introText = document.getElementById("mainHeader");
  const txt = textTyping(introText, {
    speed: [0, 250]
  });

  await txt.sleep(1000);
  await txt.typeText("Page not found ");

  const emoji = document.createElement("span");
  emoji.innerHTML = "&#128561;";
  await txt.injectHTML(emoji);
}

document.addEventListener("DOMContentLoaded", () => {
  init();
});
