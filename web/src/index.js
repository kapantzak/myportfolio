import "../public/css/main.scss";
import { textTyping } from "text-typing";
import * as OfflinePluginRuntime from "offline-plugin/runtime";

OfflinePluginRuntime.install();

let mouseoverEmoji = false;
let navigationInstance = null;
let wideScreen = true;
let defer = null;
let introTextFontLoaded = false;

async function init() {
  initDeferredAssets();
  checkScreenWidth();
  setAfterScrollEventListener();
  await initNavigation();
  setTimeout(() => {
    showLoader();
  }, 50);
  setTimeout(() => {
    showApp();
    checkInitIntroText();
    initCanvas();
  }, 2000);
}

function webFontConfigCustomEvent_fontactive_handler(e) {
  switch (((e || {}).detail || {}).familyName) {
    case "Montserrat":
      introTextFontLoaded = true;
      break;
  }
}

async function initDeferredAssets() {
  const mod = await import(
    /* webpackChunkName: "deferredHelper" */
    /* webpackMode: "lazy" */
    "./helpers/deferredHelper"
  );
  defer = mod.deferredAssets();
  defer.images();
}

function checkScreenWidth() {
  wideScreen = screen.width > 1024;
  if (!wideScreen) {
    document.body.classList.add("screen-narrow");
  }
}

async function initCanvas() {
  if (wideScreen) {
    const mod = await import(
      /* webpackChunkName: "canvasHelper" */
      /* webpackMode: "lazy" */
      "./helpers/canvasHelper"
    );
    const canvas = mod.canvasItem();
    setTimeout(() => {
      canvas.init().show();
    }, 800);
  }
}

function showLoader() {
  const loadingLogo = document.getElementById("loadingLogo");
  if (loadingLogo) {
    loadingLogo.style.opacity = 1;
    loadingLogo.style.transform = "rotate(3600deg)";
  }
}

function showApp() {
  document.getElementById("appHolder").classList.add("app-visible");
  document.getElementById("loader").style.display = "none";
}

/* Navigation ======================================================================== */

async function initNavigation() {
  const mod = await import(
    /* webpackChunkName: "navigationHelper" */
    /* webpackMode: "lazy" */
    "./helpers/navigationHelper"
  );

  navigationInstance = mod.spaNavigation();
  navigationInstance.initScrolling();
  navigationInstance.navigate();

  document.getElementById("mainNavButton").addEventListener("click", () => {
    mod.toggleMobileMenu(true);
  });

  document.getElementById("btnMenuClose").addEventListener("click", () => {
    mod.toggleMobileMenu(false);
  });

  applyChannelSplitEffectOnMenu();
}

async function applyChannelSplitEffectOnMenu() {
  const mod = await import(
    /* webpackChunkName: "textEffectsHelper" */
    /* webpackMode: "lazy" */
    "./helpers/textEffectsHelper"
  );
  document.querySelectorAll("#mainNavigation .spa-navigation").forEach(menu => {
    mod.channelSplitEffect(menu, {
      keepDimensions: true
    });
  });
}

/* =================================================================================== */

/* Intro section ===================================================================== */

function checkInitIntroText() {
  let int = setInterval(() => {
    if (introTextFontLoaded === true) {
      initIntroText();
      clearInterval(int);
      int = null;
    }
  }, 100);
  setTimeout(() => {
    if (int) {
      clearInterval(int);
      int = null;
    }
  }, 5000);
}

async function initIntroText() {
  const introText = document.getElementById("introText");
  const txt = textTyping(introText, {
    speed: [0, 250]
  });

  await txt.sleep(1000);
  await txt.typeText("Hello, I'm ");

  const greeting = getRandomGreeting();
  await txt.typeText(greeting);
  await txt.sleep(1000);
  await txt.backspace(greeting.length + 1, 50);

  await txt.typeText(" a web developer");

  const emoji = document.createElement("span");
  emoji.classList.add("intro-emoji");
  emoji.addEventListener("mouseover", emojiMouseoverHandler);
  emoji.addEventListener("mouseleave", emojiMouseleaveHandler);
  emoji.innerHTML = "&#128526;";
  await txt.injectHTML(emoji);

  await txt.sleep(1000);
  document.getElementById("sectionIntro").classList.add("section-ready");
}

async function emojiMouseoverHandler(event) {
  mouseoverEmoji = true;
  for await (const emoji of emojiesGenerator()) {
    event.target.innerHTML = emoji;
  }
}

function emojiMouseleaveHandler() {
  mouseoverEmoji = false;
}

async function* emojiesGenerator() {
  const list = [];
  for (let i = 128512; i <= 128580; i++) {
    list.push(`&#${i};`);
  }
  list.push("&#128526;");
  while (mouseoverEmoji && list.length) {
    yield await delayEmoji(list.shift());
  }
}

function delayEmoji(emoji) {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve(emoji);
    }, 100);
  });
}

function getRandomGreeting() {
  const greetings = ["Superman", "Batman", "Elvis", "John Snow"];
  return greetings[Math.round(Math.random() * (greetings.length - 1))];
}

/* =================================================================================== */

/* About section ===================================================================== */

function setupAboutSection(section, force = false) {
  if (
    !section.classList ||
    !section.classList.contains("section-loaded") ||
    force
  ) {
    initAboutSection();
  }
}

async function initAboutSection() {
  const aboutHelper = await import(
    /* webpackChunkName: "aboutHelper" */
    /* webpackMode: "lazy" */
    "./helpers/aboutHelper"
  );
  const aboutSectionInstance = aboutHelper.initAboutSectionArrows();
  if (navigationInstance) {
    navigationInstance.addAboutSectionNavigation(aboutSectionInstance);
  }

  const aboutSkillsHelper = await import(
    /* webpackChunkName: "aboutSkillsHelper" */
    /* webpackMode: "lazy" */
    "./helpers/aboutSkillsHelper"
  );
  const skills = aboutSkillsHelper.initSkills();

  aboutHelper.setAboutDetails(skills);
  aboutHelper.initAboutSectionClipPathContent();
}

/* =================================================================================== */

/* Projects section ================================================================== */

function setupProjectsSection(section, force = false) {
  if (
    !section.classList ||
    !section.classList.contains("section-loaded") ||
    force
  ) {
    initProjectsSectionHeading();
    initProjects();
  }
}

async function initProjectsSectionHeading() {
  const projectsHeading = document.getElementById("sectionProjectsHeader");
  if (!projectsHeading.classList.contains("text-typing-elem")) {
    const txt = textTyping(projectsHeading, {
      speed: [0, 250]
    });
    await txt.typeText("My Projects");
    projectsHeading.classList.add("text-typing-elem");
    channelSplitEffect(projectsHeading);
  }
}

async function initProjects() {
  import(
    /* webpackChunkName: "projectsHelper" */
    /* webpackMode: "lazy" */
    "./helpers/projectsHelper"
  ).then(mod => {
    const projectsHolder = document.getElementById("sectionProjectsListHolder");
    mod.initProjects(projectsHolder);
  });
}

/* =================================================================================== */

/* Posts section ===================================================================== */

function setupPostSection(section, force = false) {
  if (
    !section.classList ||
    !section.classList.contains("section-loaded") ||
    force
  ) {
    initPostsSectionHeading(section);
    initPostsSection();
  }
}

async function initPostsSectionHeading(targetSection) {
  const postsHeading = document.getElementById("sectionPostHeader");
  if (!postsHeading.classList.contains("text-typing-elem")) {
    const txt = textTyping(postsHeading, {
      speed: [0, 250]
    });
    await txt.typeText("My Posts");
    postsHeading.classList.add("text-typing-elem");
    targetSection.classList.add("section-ready");
    channelSplitEffect(postsHeading);
  }
}

function initPostsSection() {
  import(
    /* webpackChunkName: "postsHelper" */
    /* webpackMode: "lazy" */
    "./helpers/postsHelper"
  ).then(mod => {
    mod.injectPosts(document.getElementById("sectionPostsHolder"), wideScreen);
  });
}

/* =================================================================================== */

/* Contact section =================================================================== */

function setupContactSection(section, force = false) {
  if (
    !section.classList ||
    !section.classList.contains("section-loaded") ||
    force
  ) {
    initContactSectionHeading(section);
  }
}

async function initContactSectionHeading(targetSection) {
  const contactHeading = document.getElementById("sectionContactHeader");
  if (!contactHeading.classList.contains("text-typing-elem")) {
    const txt = textTyping(contactHeading, {
      speed: [0, 250]
    });
    await txt.typeText("kapantzak@gmail.com");
    contactHeading.classList.add("text-typing-elem");
    targetSection.classList.add("section-ready");
    channelSplitEffect(contactHeading);
  }
}

/* =================================================================================== */

/* Scrolling event handlers ========================================================== */

function setAfterScrollEventListener() {
  if (wideScreen) {
    document.addEventListener("afterScroll", afterScrollHandler);
  } else {
    setTimeout(() => {
      setupAboutSection(document.getElementById("sectionAbout"), true);
      setupProjectsSection(document.getElementById("sectionProjects"), true);
      setupPostSection(document.getElementById("sectionPosts"), true);
      setupContactSection(document.getElementById("sectionContact"), true);
    }, 500);
  }
}

function afterScrollHandler(e) {
  const targetSection = (e || {}).detail.targetSection || null;
  if (targetSection) {
    const id = targetSection.getAttribute("id");
    switch (id) {
      case "sectionAbout":
        setupAboutSection(targetSection);
        break;
      case "sectionProjects":
        setupProjectsSection(targetSection);
        break;
      case "sectionPosts":
        setupPostSection(targetSection);
        break;
      case "sectionContact":
        setupContactSection(targetSection);
        break;
    }
    targetSection.classList.add("section-loaded");
    document.body.classList.remove("elem-moving");
  }
}

/* =================================================================================== */

/* Event listeners =================================================================== */

document.addEventListener("DOMContentLoaded", () => {
  init();
});

document.addEventListener(
  "webFontConfigCustomEvent_fontactive",
  webFontConfigCustomEvent_fontactive_handler
);
