export function waitVisible(elem, callback, timeout) {
  let timer = setInterval(() => {
    if (elementVisible(elem)) {
      callback();
      clearInterval(timer);
      timer = null;
    }
  }, 10);
  const tm = timeout || 5000;
  setTimeout(() => {
    if (timer) {
      clearInterval(timer);
    }
  }, tm);
}

function elementVisible(elem) {
  return !!(
    elem.offsetWidth ||
    elem.offsetHeight ||
    elem.getClientRects().length
  );
}
