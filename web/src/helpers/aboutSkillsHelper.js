import { getSkills } from "../dataStore/skills";

export function getSkillBarWidth(skill) {
  const p = skill.percentage;
  if (p <= 0 || !p) return "0%";
  if (p > 100) return "100%";
  return `${p}%`;
}

export function initSkills() {
  const skills = getSkills();
  const skillsHolder = document.getElementById("skillsHolder");
  if (skillsHolder && skills && skills.length > 0) {
    skills.forEach(skill => {
      skillsHolder.appendChild(buildSkillItem(skill));
    });
  }
  const skillsLegend = document.getElementById("skillsLegend");

  function buildSkillItem(skill) {
    const section = document.createElement("section");
    section.classList.add("about-details-section-inner-item");
    section.classList.add("about-details-skills-item");

    const skillItemHolder = document.createElement("div");
    skillItemHolder.classList.add("skill-item-holder");

    const skillBarIndicator = document.createElement("div");
    skillBarIndicator.classList.add("skill-item-bar-indicator");
    skillBarIndicator.style.width = "0%";
    skillBarIndicator.setAttribute("data-percentage", getSkillBarWidth(skill));

    const skillTitleHolder = document.createElement("a");
    skillTitleHolder.href = skill.link;
    skillTitleHolder.setAttribute("rel", "noopener");
    skillTitleHolder.setAttribute("target", "_blank");
    skillTitleHolder.classList.add("skill-title-holder");
    skillTitleHolder.innerHTML = skill.title || "";

    skillBarIndicator.appendChild(skillTitleHolder);
    skillItemHolder.appendChild(skillBarIndicator);

    section.appendChild(skillItemHolder);
    return section;
  }

  return {
    load: function() {
      if (skillsHolder) {
        skillsHolder
          .querySelectorAll(".skill-item-bar-indicator")
          .forEach((x, index) => {
            const width = x.getAttribute("data-percentage");
            setTimeout(() => {
              x.style.width = width;
            }, index * 100);
          });
      }
      if (skillsLegend) {
        skillsLegend.classList.add("ready");
      }
    },
    reset: function() {
      if (skillsHolder) {
        skillsHolder
          .querySelectorAll(".skill-item-bar-indicator")
          .forEach(x => {
            x.style.width = "0%";
          });
      }
      if (
        skillsLegend &&
        skillsLegend.classList &&
        skillsLegend.classList.contains("ready")
      ) {
        skillsLegend.classList.remove("ready");
      }
    }
  };
}
