import { scrollToElement, scrollPixels } from "./scrollHelper";

import { updateCanvasPosition } from "./canvasHelper";
import scrollDoc from "scroll-doc";

export function spaNavigation() {
  let isMoving = false;
  let currentLocation = null;
  let aboutSectionNavigation = null;

  const menuLinks = [document.querySelector(".spa-navigation[href='home']")];
  document.querySelectorAll("#mainNavigation .spa-navigation").forEach(x => {
    menuLinks.push(x);
  });

  const sections = Array.from(
    document.querySelectorAll("a.spa-navigation")
  ).map(x => ({
    href: x.getAttribute("href"),
    element: document.getElementById(x.getAttribute("data-target"))
  }));

  const navs = document.querySelectorAll("a.spa-navigation");
  navs.forEach(nav => {
    nav.addEventListener("click", e => {
      e.preventDefault();
      const href = e.currentTarget.getAttribute("href");
      if (href && window.history) {
        window.history.pushState(null, "", href);
      }
      navigate();
    });
  });
  window.addEventListener("popstate", e => {
    navigate();
  });

  async function navigate() {
    const loc = getLocation();
    if ((currentLocation || {}).path !== loc.path) {
      isMoving = true;
      toggleMobileMenu(false);
      await scrollToElement(getSectionByLocationPath(loc.path));
      isMoving = false;
      currentLocation = loc;
    }
  }

  function getSectionByLocationPath(path) {
    const href = path ? path.substring(1) : null;
    if (href) return sections.find(x => x.href === href).element;
    return null;
  }

  function getLocation() {
    const loc = window.location;
    return {
      location: loc,
      path: loc.pathname,
      search: loc.search
    };
  }

  function initScrolling() {
    try {
      document.removeEventListener("scroll", scrollHandler);
    } catch (err) {}
    document.addEventListener("scroll", scrollHandler);

    function scrollHandler(e) {
      const scrollingElem = scrollDoc();
      updateCanvasPosition(scrollingElem);
    }

    try {
      document.removeEventListener("wheel", mousewheelHandler, {
        passive: false
      });
    } catch (err) {}
    document.addEventListener("wheel", mousewheelHandler, {
      passive: false
    });

    async function mousewheelHandler(e) {
      if (isMoving) {
        e.preventDefault();
      } else if (
        !bodyIsMoving() &&
        !elemIsAffectedByClass(e.target, "scrolling-enabled")
      ) {
        e.preventDefault();
        isMoving = true;
        setTimeout(() => {
          isMoving = false;
        }, 1000);
        if (
          elemIsAffectedByClass(e.target, "scrolling-about") &&
          aboutSectionNavigation !== null
        ) {
          if (e.deltaY > 0) {
            aboutSectionNavigation.next();
          } else {
            aboutSectionNavigation.prev();
          }
        } else {
          if (e.deltaY > 0) {
            gotoNextSection();
          } else {
            gotoPreviousSection();
          }
        }
      } else {
        // Scrolling enabled
        if (!projectDetailsOpened() && !aboutDetailsOpened()) {
          const elem = null;
          const pixels = e.deltaY > 0 ? 250 : -250;
          await scrollPixels({ elem, pixels, duration: 1000 });
        }
      }
    }

    function bodyIsMoving() {
      const bodyClasses = document.body.classList;
      return (
        bodyClasses &&
        bodyClasses.length > 0 &&
        bodyClasses.contains("elem-moving")
      );
    }

    function elemIsAffectedByClass(elem, className) {
      if (
        elem.classList &&
        elem.classList.length > 0 &&
        elem.classList.contains(className)
      )
        return true;

      let parent = elem.parentNode;
      while (parent) {
        if (
          parent.classList &&
          parent.classList.length > 0 &&
          parent.classList.contains(className)
        )
          return true;
        parent = parent.parentNode;
      }
      return false;
    }

    function projectDetailsOpened() {
      const projectDetails = document.getElementById("sectionProjectDetails");
      if (
        projectDetails.classList &&
        projectDetails.classList.contains("opened")
      )
        return true;
      return false;
    }

    function aboutDetailsOpened() {
      const aboutDetails = document.getElementById("sectionAboutDetails");
      if (aboutDetails.classList && aboutDetails.classList.contains("opened"))
        return true;
      return false;
    }

    function gotoNextSection() {
      setTargetPath("next");
    }

    function gotoPreviousSection() {
      setTargetPath("previous");
    }

    function setTargetPath(direction) {
      let target = null;
      const activeLink = document.querySelector(".spa-navigation.active");
      if (activeLink) {
        const activeIndex = menuLinks.indexOf(activeLink);
        if (direction === "next") {
          if (activeIndex < menuLinks.length - 1) {
            target = menuLinks[activeIndex + 1];
          }
        } else {
          if (activeIndex > 0) {
            target = menuLinks[activeIndex - 1];
          }
        }
      } else {
        target = menuLinks[1];
      }
      if (target) {
        const href = target.getAttribute("href");
        if (href && window.history) {
          window.history.pushState(null, "", href);
        }
        navigate();
      }
    }
  }

  return {
    initScrolling: function() {
      initScrolling();
      return this;
    },
    navigate: function() {
      navigate();
      return this;
    },
    addAboutSectionNavigation: function(instance) {
      aboutSectionNavigation = instance;
    }
  };
}

export function toggleMobileMenu(open) {
  const nav = document.getElementById("mainNavigation");
  if (open) {
    nav.classList.add("navigation-visible");
  } else {
    nav.classList.add("navigation-invisible");
    nav.classList.remove("navigation-visible");
    setTimeout(() => {
      nav.classList.remove("navigation-invisible");
    }, 250);
  }
}
