export function canvasItem() {
  const w = document.body.offsetWidth;
  const h = document.body.offsetHeight;

  const canvas = document.getElementById("canvas");
  const ctx = canvas.getContext("2d");
  ctx.canvas.width = w;
  ctx.canvas.height = h;

  function draw() {
    // #1
    ctx.beginPath();
    ctx.moveTo(w * 0.06, 0);
    ctx.quadraticCurveTo(w * 0.7, h * 0.1, 0, h * 0.2);
    ctx.lineTo(0, h * 0.19);
    ctx.quadraticCurveTo(w * 0.55, h * 0.1, 0, 0);
    ctx.lineTo(5, 0);
    ctx.fillStyle = "rgba(59, 0, 235, 0.8)";
    ctx.fill();

    ctx.beginPath();
    ctx.moveTo(w * 0.05, 0);
    ctx.quadraticCurveTo(w * 0.65, h * 0.1, 0, h * 0.2);
    ctx.lineTo(0, h * 0.19);
    ctx.quadraticCurveTo(w * 0.5, h * 0.1, 0, 0);
    ctx.lineTo(5, 0);
    ctx.fillStyle = "rgba(214, 2, 221, 0.8)";
    ctx.fill();

    // #2
    ctx.beginPath();
    ctx.moveTo(w, h * 0.1);
    ctx.quadraticCurveTo(w * 0.27, h * 0.2, w, h * 0.4);
    ctx.lineTo(w, h * 0.39);
    ctx.quadraticCurveTo(w * 0.37, h * 0.2, w, h * 0.11);
    ctx.lineTo(w, h * 0.1);
    ctx.fillStyle = "rgba(214, 2, 221, 0.8)";
    ctx.fill();

    ctx.beginPath();
    ctx.moveTo(w, h * 0.1);
    ctx.quadraticCurveTo(w * 0.3, h * 0.2, w, h * 0.4);
    ctx.lineTo(w, h * 0.39);
    ctx.quadraticCurveTo(w * 0.4, h * 0.2, w, h * 0.11);
    ctx.lineTo(w, h * 0.1);
    ctx.fillStyle = "rgba(59, 0, 235, 0.8)";
    ctx.fill();

    // #3
    ctx.beginPath();
    ctx.moveTo(0, h * 0.25);
    ctx.quadraticCurveTo(w * 0.7, h * 0.5, 0, h * 0.6);
    ctx.lineTo(0, h * 0.59);
    ctx.quadraticCurveTo(w * 0.55, h * 0.5, 0, h * 0.27);
    ctx.lineTo(0, h * 0.3);
    ctx.fillStyle = "rgba(59, 0, 235, 0.8)";
    ctx.fill();

    ctx.beginPath();
    ctx.moveTo(0, h * 0.25);
    ctx.quadraticCurveTo(w * 0.75, h * 0.5, 0, h * 0.6);
    ctx.lineTo(0, h * 0.59);
    ctx.quadraticCurveTo(w * 0.6, h * 0.5, 0, h * 0.27);
    ctx.lineTo(0, h * 0.3);
    ctx.fillStyle = "rgba(214, 2, 221, 0.8)";
    ctx.fill();
  }

  return {
    init: function() {
      draw();
      return this;
    },
    show: function() {
      canvas.classList.add("canvas-ready");
      return this;
    }
  };
}

export function updateCanvasPosition(elem) {
  const canvas = document.getElementById("canvas");
  canvas.style.top = `${elem.scrollTop * 0.5}px`;
}
