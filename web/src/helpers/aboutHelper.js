import { getArrowPath } from "./svgHelper";
import { CountUp } from "countup.js";

export function initAboutSectionArrows() {
  const sectionsOrder = {
    name: "1",
    workDays: "2",
    HTML: "3",
    javascript: "4",
    cSharp: "4"
  };
  const sectionsInitialized = [];
  const countUp_aboutDaysOfWork = getAboutDaysOfWorkCountUp();
  const countUp_aboutHTMLCSS = getYearsFromDate("aboutHTMLCSS", 2009);
  const countUp_aboutYearsOfJavascript = getYearsFromDate(
    "aboutJavascriptYears",
    2010
  );

  document
    .querySelectorAll("#sectionAboutInner .section-about-item-arrow > svg")
    .forEach(x => {
      ["arrow-red", "arrow-green", "arrow-blue"].forEach(className => {
        x.appendChild(getArrowPath(className));
        setTimeout(() => {
          x.classList.add("svg-ready");
        }, 0);
      });

      x.closest(".section-about-item").addEventListener(
        "click",
        handleArrowClick
      );
    });

  sectionAboutInnerHandler(
    document.querySelector(".section-about-item.active")
  );

  updateAboutPaging();

  function handleArrowClick(e, direction = "next") {
    if (!e || (e.target && e.target.nodeName !== "A")) {
      const thisSection = e
        ? e.currentTarget
        : document.querySelector(".section-about-item.active");
      const directionClass =
        direction === "next" ? "moving-next" : "moving-prev";
      thisSection.classList.add("section-moving-out");
      thisSection.classList.add(directionClass);
      setTimeout(() => {
        const next =
          direction === "next"
            ? getNextAboutInnerSection()
            : getPreviousAboutInnerSection();
        document.querySelectorAll(".section-about-item.active").forEach(x => {
          x.classList.remove("active");
        });
        next.classList.add("active");
        thisSection.classList.remove("section-moving-out");
        thisSection.classList.remove(directionClass);
        next.classList.add("section-moving-in");
        next.classList.add(directionClass);
        initAboutDaysOfWorkCount(next);
        initAboutYearsOfHTMLCSS(next);
        initAboutYearsOfJavascript(next);
        setTimeout(() => {
          next.classList.remove("section-moving-in");
          next.classList.remove(directionClass);
          if (!sectionsInitialized.includes(next)) {
            sectionAboutInnerHandler(next);
            sectionsInitialized.push(next);
          }
          updateAboutPaging();
        }, 800);
      }, 800);
    }
  }

  function getNextAboutInnerSection() {
    const sections = document.querySelectorAll(".section-about-item");
    const active = document.querySelector(".section-about-item.active");
    const next = active.nextElementSibling;
    return next || sections[0];
  }

  function getPreviousAboutInnerSection() {
    const sections = document.querySelectorAll(".section-about-item");
    const active = document.querySelector(".section-about-item.active");
    const prev = active.previousElementSibling;
    return prev || sections[sections.length - 1];
  }

  function sectionAboutInnerHandler(elem) {
    const initialX = 80;
    const initialY = 0;
    const center = {
      x: elem.offsetLeft + elem.offsetWidth / 2,
      y: elem.offsetTop + elem.offsetHeight / 2
    };
    const arrowRed = elem.querySelector(".arrow-red");
    const arrowGreen = elem.querySelector(".arrow-green");
    const arrowBlue = elem.querySelector(".arrow-blue");

    elem.addEventListener("mousemove", mousemoveHandler);
    elem.addEventListener("mouseleave", mouseleaveHanlder);

    function mousemoveHandler(e) {
      const x = e.clientX;
      const y = e.clientY;
      const xp = (center.x - x) / center.x;
      const yp = (center.y - y) /*(y + window.pageYOffset)*/ / center.y;

      const newRedStyle = calculateTransform(
        { x: xp, y: yp, xMax: 10, yMax: 10 },
        1
      );
      arrowRed.style.transform = newRedStyle;
      arrowRed.style.webkitTransform = newRedStyle;

      const newGreenStyle = calculateTransform(
        { x: xp, y: yp, xMax: 5, yMax: 5 },
        -1
      );
      arrowGreen.style.transform = newGreenStyle;
      arrowGreen.style.webkitTransform = newGreenStyle;

      const newBlueStyle = calculateTransform(
        { x: xp, y: yp, xMax: 15, yMax: 15 },
        -1
      );
      arrowBlue.style.transform = newBlueStyle;
      arrowBlue.style.webkitTransform = newBlueStyle;
    }

    function mouseleaveHanlder() {
      const resetStyle = resetTransform();
      arrowRed.style.transform = resetStyle;
      arrowRed.style.webkitTransform = resetStyle;
      arrowGreen.style.transform = resetStyle;
      arrowGreen.style.webkitTransform = resetStyle;
      arrowBlue.style.transform = resetStyle;
      arrowBlue.style.webkitTransform = resetStyle;
    }

    function resetTransform() {
      return `translate(${initialX}%, ${initialY}%)`;
    }

    function calculateTransform({ x, y, xMax, yMax }, sign) {
      const newX = initialX + x * xMax * sign;
      const newY = initialY + y * yMax * sign;
      return `translate(${newX}%, ${newY}%)`;
    }
  }

  function updateAboutPaging() {
    const pagingHolder = document.getElementById("sectionAboutPaging");
    const active = document.querySelector(".section-about-item.active");
    const order = active ? active.getAttribute("data-order") : null;
    const allCount = document.querySelectorAll(".section-about-item").length;
    pagingHolder.innerHTML = `${order}/${allCount}`;
  }

  function initAboutDaysOfWorkCount(section) {
    if (
      countUp_aboutDaysOfWork &&
      section.getAttribute("data-order") === sectionsOrder.workDays
    ) {
      countUp_aboutDaysOfWork.start();
    } else {
      countUp_aboutDaysOfWork.reset();
    }
  }

  function getDaysElapsed(dateFrom) {
    return Math.round(
      Math.abs(dateFrom.getTime() - Date.now()) / (24 * 60 * 60 * 1000)
    );
  }

  function getAboutDaysOfWorkCountUp() {
    const days = getDaysElapsed(new Date(2014, 8, 8, 0, 0, 0));
    const startVal = Math.round(days * 0.9);
    return new CountUp("aboutDaysOfWork", days, {
      startVal: startVal,
      duration: 1.3
    });
  }

  function initAboutYearsOfHTMLCSS(section) {
    if (
      countUp_aboutHTMLCSS &&
      section.getAttribute("data-order") === sectionsOrder.HTML
    ) {
      countUp_aboutHTMLCSS.start();
    } else {
      countUp_aboutHTMLCSS.reset();
    }
  }

  function initAboutYearsOfJavascript(section) {
    if (
      countUp_aboutYearsOfJavascript &&
      section.getAttribute("data-order") === sectionsOrder.javascript
    ) {
      countUp_aboutYearsOfJavascript.start();
    } else {
      countUp_aboutYearsOfJavascript.reset();
    }
  }

  function getYearsFromDate(elemId, fromYear = new Date().getFullYear() - 1) {
    if (document.getElementById(elemId)) {
      const years = new Date().getFullYear() - fromYear;
      return new CountUp(elemId, years, {
        duration: 3
      });
    }
    return null;
  }

  document
    .getElementById("sectionAboutDetails")
    .addEventListener("scroll", aboutDetailsScrollHander);

  function aboutDetailsScrollHander(e) {
    const elem = e.currentTarget;
    const scrollTop = elem.scrollTop;
    document
      .querySelectorAll("#sectionAboutDetailsContent .parallax-element")
      .forEach(x => {
        x.style.top = `${scrollTop * 0.5}px`;
      });
  }

  return {
    next: function() {
      handleArrowClick(null, "next");
    },
    prev: function() {
      handleArrowClick(null, "prev");
    }
  };
}

export function setAboutDetails(skills) {
  const btn = document.getElementById("sectionAboutViewDetailsHolder");
  const holder = document.getElementById("sectionAboutDetails");
  const btnClose = document.getElementById("btnAboutDetailsCloseHolder");
  if (btn && holder && btnClose) {
    btn.addEventListener("click", sectionAboutViewDetailsClickHandler);

    function sectionAboutViewDetailsClickHandler() {
      holder.classList.add("opened");
      setTimeout(() => {
        skills.load();
      }, 800);
    }

    btnClose.addEventListener("click", btnCloseClickHandler);

    function btnCloseClickHandler() {
      if (holder.classList) {
        holder.classList.remove("opened");
      }
      holder.classList.add("closed");
      setTimeout(() => {
        holder.classList.remove("closed");
        holder.scrollTop = 0;
        document.body.style.overflow = "";
        skills.reset();
      }, 800);
    }
  }
}

export function initAboutSectionClipPathContent() {
  document.querySelectorAll(".clip-path-content").forEach(section => {
    const cloned = document.createElement("section");
    cloned.classList.add("clip-path-cloned-section");
    cloned.innerHTML = section.innerHTML;
    section.appendChild(cloned);
    section.addEventListener("mouseenter", clipPathContentMouseenterHandler);
    section.addEventListener("mouseleave", clipPathContentMouseleaveHandler);
  });

  function clipPathContentMouseenterHandler(e) {
    const cloned = e.currentTarget.querySelector(".clip-path-cloned-section");
    if (cloned) {
      cloned.style.display = "block";
      if (cloned.classList && cloned.classList.length > 0) {
        cloned.classList.remove("closed");
      }
      cloned.classList.add("opened");
    }
  }

  function clipPathContentMouseleaveHandler(e) {
    const cloned = e.currentTarget.querySelector(".clip-path-cloned-section");
    if (cloned) {
      cloned.classList.remove("opened");
      cloned.classList.add("closed");
      setTimeout(() => {
        if (!cloned.classList.contains("opened")) {
          cloned.style.display = "none";
        }
      }, 800);
    }
  }
}
