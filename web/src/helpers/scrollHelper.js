import scroll from "scroll";
import scrollDoc from "scroll-doc";
import ease from "ease-component";
import { waitVisible } from "./visibilityHelper";

/**
 * Scroll to specified section
 * @param {HTMLElement} elem A .main-content-section element
 */
export function scrollToElement(elem) {
  if (elem) {
    waitVisible(
      elem,
      () => {
        const beforeScrollEvent = new CustomEvent("beforeScroll", {
          detail: {
            fromSection: document.querySelector(".main-content-section.active")
          }
        });
        document.dispatchEvent(beforeScrollEvent);
        const scrollingElem = scrollDoc();
        const pos = window.pageYOffset + elem.getBoundingClientRect().top;
        const diff = Math.abs(window.pageYOffset - pos);
        const duration = Math.max(500, Math.min(diff, 2000));
        return new Promise(resolve => {
          scroll.top(
            scrollingElem,
            pos,
            {
              duration,
              ease: ease.inOutExpo
            },
            (err, scrollTop) => {
              setActiveElementsByActiveSection(elem);
              dispatchAfterScrollEvent(elem);
              resolve("Success");
            }
          );
        });
      },
      10000
    );
  } else {
    return new Promise(resolve => resolve(null));
  }
}

/**
 * Scroll for specified pixels
 * @param {any} object Object of type { pixels: number, duration: number }
 */
export function scrollPixels({ elem, pixels, duration }) {
  return new Promise(resolve => {
    const newPos = window.pageYOffset + pixels;
    const scrollElem = elem || scrollDoc();
    scroll.top(
      scrollElem,
      newPos,
      {
        duration,
        ease: ease.outExpo
      },
      (err, scrollTop) => {
        resolve("Success");
      }
    );
  });
}

/**
 * Dispatch "afterScroll" custom event passing the provided section
 * @param {HTMLElement} elem A .main-content-section element
 */
export function dispatchAfterScrollEvent(elem) {
  const afterScrollEvent = new CustomEvent("afterScroll", {
    detail: {
      targetSection: elem
    }
  });
  document.dispatchEvent(afterScrollEvent);
}

/**
 * Set active section and active menu link
 * @param {HTMLElement} elem A .main-content-section element
 */
function setActiveElementsByActiveSection(elem) {
  document.querySelectorAll(".main-content-section").forEach(x => {
    x.classList.remove("active");
  });
  elem.classList.add("active");
  document.querySelectorAll(".spa-navigation").forEach(x => {
    x.classList.remove("active");
  });
  const link = document.querySelector(
    `.spa-navigation[data-target='${elem.getAttribute("id")}']`
  );
  if (link) {
    link.classList.add("active");
  }
}
