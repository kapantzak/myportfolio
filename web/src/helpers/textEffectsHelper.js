export function channelSplitEffect(textHolder, { keepDimensions } = {}) {
  if (textHolder) {
    textHolder.classList.add("channel-split-wrapper");
    const inner = textHolder.innerHTML;
    textHolder.innerHTML = "";

    if (keepDimensions) {
      const spanHidden = document.createElement("span");
      spanHidden.classList.add("channel-split-static");
      spanHidden.innerHTML = inner;
      textHolder.appendChild(spanHidden);
    }

    ["red", "green", "blue"].forEach(x => {
      const span = document.createElement("span");
      span.classList.add("channel-split");
      span.classList.add(`channel-split-${x}`);
      span.innerHTML = inner;
      textHolder.appendChild(span);
    });
  }
}
