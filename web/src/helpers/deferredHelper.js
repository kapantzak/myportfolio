export function deferredAssets() {
  function getImages() {
    document.querySelectorAll(".deferred-image").forEach(x => {
      const src = x.getAttribute("data-src");
      if (src) {
        const timeout = Number(x.getAttribute("data-timeout"));
        const t = !isNaN(timeout) ? timeout : 500;
        setTimeout(() => {
          try {
            x.src = src;
          } catch (e) {
            console.warn("Could not set src attribute to element");
          }
        }, t);
      }
    });
  }

  return {
    images: () => {
      getImages();
    }
  };
}
