import axios from "axios";

export async function injectPosts(postsHolder, renderNextPost = true) {
  const posts = await getPosts();
  if (posts === null) {
    postsHolder.innerHTML = "<h1>No posts found</h1>";
  } else {
    posts.forEach(post => {
      postsHolder.appendChild(buildPost(post));
    });
    if (renderNextPost && posts.length % 2 !== 0) {
      postsHolder.appendChild(buildNextPost());
    }
  }
}

function buildPost(postData) {
  const postWrapper = document.createElement("article");
  postWrapper.classList.add("post-wrapper");

  const delay = getRandomDelay(0, 800);
  postWrapper.style.transitionDelay = `${delay}ms`;

  if (postData.cover_image) {
    const postCoverImg = document.createElement("img");
    postCoverImg.classList.add("post-image");
    postCoverImg.src = postData.cover_image;
    postWrapper.appendChild(postCoverImg);
    postWrapper.classList.add("post-wrapper-image");
  } else {
    postWrapper.classList.add("post-wrapper-noimage");
  }

  const postTitleHolder = document.createElement("h2");
  postTitleHolder.classList.add("post-title-holder");
  postTitleHolder.innerHTML = postData.title;
  postWrapper.appendChild(postTitleHolder);

  const postDateHolder = document.createElement("h5");
  postDateHolder.classList.add("post-date-holder");
  const date = new Date(postData.published_at);
  postDateHolder.innerHTML = `${date.getDate()}/${date.getMonth() +
    1}/${date.getFullYear()}`;
  postWrapper.appendChild(postDateHolder);

  const postOverlayAnchor = document.createElement("a");
  postOverlayAnchor.classList.add("post-overlay-anchor");
  postOverlayAnchor.href = postData.canonical_url;
  postOverlayAnchor.setAttribute("target", "_blank");
  postOverlayAnchor.setAttribute("rel", "noopener");

  const postOverlayInner = document.createElement("div");
  postOverlayInner.classList.add("post-overlay-inner");

  const postOverlayTitle = document.createElement("h2");
  postOverlayTitle.classList.add("post-overlay-title");
  postOverlayTitle.innerHTML = postData.title;

  const postOverlaySub = document.createElement("p");
  postOverlaySub.classList.add("post-overlay-sub");
  postOverlaySub.innerHTML = `${postData.positive_reactions_count} &#128077;`;

  postOverlayInner.appendChild(postOverlayTitle);
  postOverlayInner.appendChild(postOverlaySub);

  postOverlayAnchor.appendChild(postOverlayInner);

  postWrapper.appendChild(postOverlayAnchor);

  return postWrapper;
}

function buildNextPost() {
  const postWrapper = document.createElement("article");
  postWrapper.classList.add("post-wrapper");
  postWrapper.classList.add("post-wrapper-next");

  const delay = getRandomDelay(0, 800);
  postWrapper.style.transitionDelay = `${delay}ms`;

  const inner = document.createElement("h1");
  inner.classList.add("typed-text");
  inner.classList.add("text-sm");
  inner.innerHTML = ">My next post";

  postWrapper.appendChild(inner);

  return postWrapper;
}

function getRandomDelay(min, max) {
  return Math.round(Math.random() * (max - min) + min);
}

async function getPosts() {
  try {
    const posts = await axios.get(
      "https://dev.to/api/articles?username=kapantzak"
    );
    return posts.data;
  } catch (e) {
    return null;
  }
}
