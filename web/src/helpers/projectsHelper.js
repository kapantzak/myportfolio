import { getProjects } from "../dataStore/projects";

export function initProjects(elem) {
  let projectListItemHovered = false;

  if (elem) {
    getProjects().forEach((x, index) => {
      const timeout = 150 * index;
      elem.appendChild(buildProjectItem(x, timeout));
    });
    elem.appendChild(buildProjectImageHolder());
  }

  function buildProjectImageHolder() {
    const holder = document.createElement("div");
    holder.id = "projectListImageHolder";
    holder.classList.add("project-list-item-image-holder");
    holder.style.display = "none";

    const img = document.createElement("img");
    img.id = "projectListImage";
    img.classList.add("project-list-item-image");

    holder.appendChild(img);
    return holder;
  }

  function buildProjectItem(project, timeout) {
    const item = document.createElement("section");
    item.classList.add("project-list-item");
    item.onclick = e => onItemClickHandler(e, project);
    if (project.img) {
      item.onmouseenter = e => {
        onItemMouseenterHandler(e, project);
      };
      item.onmouseleave = onItemMouseleaveHandler;
      item.onmousemove = onItemMousemoveHandler;
    }

    const projectTitle = document.createElement("h3");
    projectTitle.innerHTML = project.title;

    item.appendChild(projectTitle);
    setTimeout(() => {
      item.classList.add("item-ready");
    }, timeout);
    return item;
  }

  function onItemClickHandler(e, project) {
    document.body.style.overflow = "hidden";
    setProjectDetailsContent(project);
    resetImage();
    const holder = document.getElementById("sectionProjectDetails");
    holder.classList.add("opened");
  }

  function onItemMouseenterHandler(e, project) {
    projectListItemHovered = true;
    document.getElementById("projectListImageHolder").style.display = "flex";
    document.getElementById("projectListImage").src = project.img;
    setTimeout(() => {
      if (projectListItemHovered) {
        document
          .getElementById("sectionProjectsListHolder")
          .classList.add("item-hover");
      }
    }, 150);
  }

  function onItemMouseleaveHandler(e) {
    resetImage();
  }

  function resetImage() {
    projectListItemHovered = false;
    document
      .getElementById("sectionProjectsListHolder")
      .classList.remove("item-hover");
    document.getElementById("projectListImageHolder").style.display = "none";
    document.getElementById("projectListImage").src = "";
  }

  function onItemMousemoveHandler(e) {
    const img = document.getElementById("projectListImageHolder");
    img.style.top = `${e.clientY}px`;
    img.style.left = `${e.clientX}px`;
  }

  document
    .getElementById("btnProjectDetailsCloseHolder")
    .addEventListener("click", projectDetailsCloseButtonClickHandler);

  function projectDetailsCloseButtonClickHandler(e) {
    const div = document.getElementById("sectionProjectDetails");
    if (div.classList) {
      div.classList.remove("opened");
    }
    div.classList.add("closed");
    setTimeout(() => {
      div.classList.remove("closed");
      div.scrollTop = 0;
      document.body.style.overflow = "";
    }, 800);
  }

  function setProjectDetailsContent(project) {
    document.getElementById("projectDetailTitle").innerHTML = project.title;
    document.getElementById(
      "projectDetailDescription"
    ).innerHTML = `<p>${project.descr}</p>`;
    getProjectDetailsImages(project);
    getProjectDetailsStack(project);
    getProjectDetailsLinks(project);
  }

  function getProjectDetailsStack(project) {
    const stackHolder = document.getElementById("projectDetailStack");
    stackHolder.innerHTML = "";
    if (project.stack && project.stack.length > 0) {
      project.stack.forEach(x => {
        const div = document.createElement("div");
        div.classList.add("badge-item");
        div.innerHTML = x;
        stackHolder.appendChild(div);
      });
    }
  }

  function getProjectDetailsLinks(project) {
    const linksHolder = document.getElementById("projectDetailLinks");
    linksHolder.innerHTML = "";
    if (project.links && project.links.length > 0) {
      project.links.forEach(x => {
        if (x.url && x.type && x.text) {
          const div = document.createElement("div");
          div.classList.add("project-details-link-badge-holder");

          const a = document.createElement("a");
          a.classList.add("project-details-link-badge");
          a.href = x.url;
          a.setAttribute("target", "_blank");
          a.setAttribute("title", x.text);
          a.setAttribute("rel", "noopener");
          const icon = getLinkTypeIcon(x.type);
          if (icon) {
            a.appendChild(icon);
          }

          const textHolder = document.createElement("span");
          textHolder.classList.add("project-details-link-text-holder");
          textHolder.innerHTML = x.text;
          a.appendChild(textHolder);

          div.appendChild(a);
          linksHolder.appendChild(div);
        }
      });
    }
  }

  function getLinkTypeIcon(type) {
    let className = null;
    switch (type) {
      case "GitHub":
        className = "fab fa-github-square";
        break;
      case "GitLab":
        className = "fab fa-gitlab";
        break;
      case "npm":
        className = "fab fa-npm";
        break;
      case "demo":
        className = "fas fa-desktop"; //"fas fa-globe";
        break;
    }
    if (className) {
      const icon = document.createElement("i");
      className.split(" ").forEach(c => {
        icon.classList.add(c);
      });
      return icon;
    }
    return null;
  }

  function getProjectDetailsImages(project) {
    const imagesHolder = document.getElementById("projectDetailSectionImage");
    imagesHolder.innerHTML = "";
    if (project.images && project.images.length > 0) {
      project.images.forEach(x => {
        if (x.src) {
          imagesHolder.appendChild(getImage(x));
        } else if (x.text) {
          imagesHolder.appendChild(getText(x));
        } else if (x.code) {
          imagesHolder.appendChild(getCode(x));
        }
      });
      imagesHolder.appendChild(getParallaxElement(project));
    }
  }

  function getParallaxElement(project) {
    const div = document.createElement("div");
    div.classList.add("parallax-element");
    div.innerHTML = project.title;
    return div;
  }

  function getImage(data) {
    const div = document.createElement("div");
    div.classList.add("project-detail-default-holder");
    div.classList.add("project-detail-image-holder");
    const orientationClass = data.orientation
      ? `orientation-${data.orientation}`
      : "orientation-horizontal";
    div.classList.add(orientationClass);
    const img = document.createElement("img");
    img.classList.add("project-detail-image");
    img.src = data.src;
    const txt = document.createElement("div");
    txt.classList.add("project-detail-text-holder");
    txt.innerHTML = data.text;
    div.appendChild(img);
    div.appendChild(txt);
    return div;
  }

  function getText(data) {
    const div = document.createElement("div");
    div.classList.add("project-detail-default-holder");
    div.classList.add("project-detail-imageText-holder");
    const txt = document.createElement("span");
    txt.classList.add("project-detail-imagetext");
    txt.innerHTML = data.text;
    div.appendChild(txt);
    return div;
  }

  function getCode(data) {
    const div = document.createElement("div");
    div.classList.add("project-detail-default-holder");
    div.classList.add("project-detail-imageCode-holder");
    const code = document.createElement("pre");
    code.classList.add("project-detail-imagecode");
    code.innerHTML = data.code;
    div.appendChild(code);
    return div;
  }

  document
    .getElementById("sectionProjectDetails")
    .addEventListener("scroll", projectDetailsScrollHander);

  function projectDetailsScrollHander(e) {
    const elem = e.currentTarget;
    const scrollTop = elem.scrollTop;
    document
      .querySelectorAll("#sectionProjectDetailsContent .parallax-element")
      .forEach(x => {
        x.style.top = `${scrollTop * 0.5}px`;
      });
  }
}
